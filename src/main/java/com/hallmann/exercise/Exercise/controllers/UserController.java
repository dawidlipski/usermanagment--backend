package com.hallmann.exercise.Exercise.controllers;

import java.util.List;

import org.jboss.logging.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.hallmann.exercise.Exercise.DTO.UserDTO;
import com.hallmann.exercise.Exercise.entities.Statistics;
import com.hallmann.exercise.Exercise.entities.SubStatistics;
import com.hallmann.exercise.Exercise.entities.Team;
import com.hallmann.exercise.Exercise.entities.User;
import com.hallmann.exercise.Exercise.services.UserServices;

@Controller
@CrossOrigin(origins = "*")
@RequestMapping(path = "/user")
public class UserController {

	private static final Logger LOG = Logger.getLogger(UserController.class);

	@Autowired
	private UserServices userServices;

	@PostMapping(path = "/add")
	public ResponseEntity<String> addNewUser(@RequestBody User user) {
		LOG.info(String.format("Adding new user: %s", user));
		return userServices.addNewUser(user);
	}

	@PostMapping("/add/users/team/{id}")
	public ResponseEntity<String> addUsersToTeam(@PathVariable Integer id, @RequestBody String users) {
		return userServices.addUsersToTeam(id, users);
	}

	@GetMapping("/users/outside/team/{id}")
	public ResponseEntity<List<User>> getUsersNotInTeam(@PathVariable Integer id) {
		return userServices.getUsersOutsideTeam(id);
	}

	@DeleteMapping("/{userID}/delete/team/{teamID}")
	public ResponseEntity<String> deleteUserFromTeam(@PathVariable Integer userID, @PathVariable Integer teamID) {
		return userServices.deleteUserFromTeam(userID, teamID);
	}

	@PostMapping("/{userID}/add/team/{teamID}")
	public ResponseEntity<String> addUserToTeam(@PathVariable Integer userID, @PathVariable Integer teamID) {
		return userServices.addUserToTeam(userID, teamID);
	}

	@GetMapping("/{id}/teams")
	public ResponseEntity<List<Team>> getUserTeams(@PathVariable Integer id) {
		return userServices.getUserTeams(id);
	}

	@PutMapping("/update/{id}")
	public ResponseEntity<String> updateUser(@PathVariable Integer id, @RequestBody UserDTO user) {
		return userServices.updateUser(id, user);
	}

	@DeleteMapping(path = "/delete/{id}")
	public ResponseEntity<String> deleteUser(@PathVariable Integer id) {
		return userServices.deleteUser(id);
	}
	
	@PostMapping(path = "/update/stats/{userId}/{tournamentType}/{gameType}/won")
	public ResponseEntity<String> addTournamentWon(@PathVariable Integer userId, @PathVariable Integer tournamentType, @PathVariable Integer gameType){
		return userServices.addTournamentWon(userId, tournamentType, gameType);
	}
	
	@PostMapping(path = "/update/stats/{userId}/{tournamentType}/{gameType}/played")
	public ResponseEntity<String> addTournamentPlayed(@PathVariable Integer userId, @PathVariable Integer tournamentType, @PathVariable Integer gameType){
		return userServices.addTournamentPlayed(userId, tournamentType, gameType);
	}
	
	@GetMapping(path = "/get/substats/{userId}/{tournamentType}/{gameType}")
	public ResponseEntity<SubStatistics> findSubStatsByIds(@PathVariable Integer userId, @PathVariable Integer tournamentType, @PathVariable Integer gameType){
		return userServices.findSubStatsByIds(userId, tournamentType, gameType);
	}

	@GetMapping(path = "/get/stats/{id}")
	public ResponseEntity<Statistics> findStatsByUserId(@PathVariable Integer id) {
		return userServices.findStatsByUserId(id);
	}

	@GetMapping(path = "/get/{id}")
	public ResponseEntity<User> findById(@PathVariable Integer id) {
		return userServices.findById(id);
	}
	
	@GetMapping(path= "/get/by-cardid/{cardID}")
	public ResponseEntity<User> getUserByCardId(@PathVariable String cardID){
		return userServices.getUserByCardId(cardID);
	}
	
	@GetMapping(path= "/get/by-nick/{nick}")
	public ResponseEntity<User> getUserByNick(@PathVariable String nick){
		return userServices.getUserByNick(nick);
	}
	
	@GetMapping(path="/bots")
	public ResponseEntity<List<User>> getBots(){
		return userServices.getBots();
	}
	
	@GetMapping(path="/bot-url/{id}")
	public ResponseEntity<String> getBotUrl(@PathVariable Integer id){
		return userServices.getBotUrl(id);
	}
	
	@GetMapping(path= "/get/by-contain-nick/{nick}")
	public ResponseEntity<List<User>> getUserByContainNick(@PathVariable String nick){
		return userServices.getUserByContainNick(nick);
	}
	
	@GetMapping(path = "/all")
	public ResponseEntity<Iterable<User>> getAllUsers() {
		return userServices.getAllUsers();
	}

}
