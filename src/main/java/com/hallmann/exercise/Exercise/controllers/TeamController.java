package com.hallmann.exercise.Exercise.controllers;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.hallmann.exercise.Exercise.DTO.TeamDTO;
import com.hallmann.exercise.Exercise.entities.Team;
import com.hallmann.exercise.Exercise.entities.User;
import com.hallmann.exercise.Exercise.services.TeamServices;

@Controller
@CrossOrigin(origins = "*")
@RequestMapping(path = "/team")
public class TeamController {

	@Autowired
	private TeamServices teamServices;

	@PostMapping(path ="/add/with-users")
	public ResponseEntity<String> addTeamWithUsers(@RequestBody Team team){
		return teamServices.addTeamWithUsers(team);
	}
	
	@PutMapping("/update/{id}")
	public ResponseEntity<String> updateTeam(@PathVariable Integer id, @RequestBody TeamDTO team) {
		return teamServices.updateTeam(id, team);
	}

	@DeleteMapping(path = "/delete/{id}")
	public ResponseEntity<String> deleteTeam(@PathVariable Integer id) {
		return teamServices.deleteTeam(id);
	}

	@GetMapping(path = "/{id}/users")
	public ResponseEntity<List<User>> getTeamUsers(@PathVariable Integer id) {
		return teamServices.getTeamUsers(id);
	}

	@GetMapping(path = "/get/{id}")
	public ResponseEntity<Team> findById(@PathVariable Integer id) {
		return teamServices.findById(id);
	}

	@GetMapping(path = "/all")
	public ResponseEntity<Iterable<Team>> getAllTeams()   {
		return teamServices.getAllUsers();
	}
	
	@PostMapping(path = "/update/stats/{teamId}/{tournamentType}/{gameType}/won")
	public ResponseEntity<String> updateTeamStatsWon(@PathVariable Integer teamId, @PathVariable Integer tournamentType, @PathVariable Integer gameType){
		return teamServices.updateTeamStatsWon(teamId, tournamentType, gameType);
	}
	
	@PostMapping(path = "/update/stats/{teamId}/{tournamentType}/{gameType}/played")
	public ResponseEntity<String> updateTeamStatsPlayed(@PathVariable Integer teamId, @PathVariable Integer tournamentType, @PathVariable Integer gameType){
		return teamServices.updateTeamStatsPlayed(teamId, tournamentType, gameType);
	}
	
	@PostMapping(path="/get-by-users")
	public ResponseEntity<Integer> getTeamsByListWithUsers(@RequestBody List<Integer> usersId){
		return teamServices.getTeamByUserList(usersId);
	}
}
