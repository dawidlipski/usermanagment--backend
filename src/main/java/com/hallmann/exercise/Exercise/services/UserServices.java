package com.hallmann.exercise.Exercise.services;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.jboss.logging.Logger;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.hallmann.exercise.Exercise.DTO.UserDTO;
import com.hallmann.exercise.Exercise.controllers.UserController;
import com.hallmann.exercise.Exercise.entities.Statistics;
import com.hallmann.exercise.Exercise.entities.SubStatistics;
import com.hallmann.exercise.Exercise.entities.Team;
import com.hallmann.exercise.Exercise.entities.User;
import com.hallmann.exercise.Exercise.repositories.StatisticsRepository;
import com.hallmann.exercise.Exercise.repositories.SubStatisticsRepository;
import com.hallmann.exercise.Exercise.repositories.TeamRepository;
import com.hallmann.exercise.Exercise.repositories.UserRepository;

@Service
public class UserServices {

	private static final Logger LOG = Logger.getLogger(UserController.class);
	
	@Autowired
	private UserRepository userRepo;

	@Autowired
	private TeamRepository teamRepo;
	
	@Autowired
	private StatisticsRepository statRepo;
	
	@Autowired
	private SubStatisticsRepository subStatRepo;

	// adds user to database
	public ResponseEntity<String> addNewUser(User user) {
		User newUser = new User();
		Optional<User> cardId= userRepo.findUserByCardId(user.getCardId());
		Statistics stats = statRepo.save(new Statistics());
		
		int numOfTournaments = 2;
		int numOfGames = 3;
		
		List<SubStatistics> subStats = new ArrayList<>();
		for(int i = 0; i < numOfTournaments; i++) {
			for(int j = 0; j < numOfGames; j++) {
				SubStatistics subStat = subStatRepo.save(new SubStatistics(i, j));
				subStats.add(subStat);
				
			}
		}
		
		stats.setSubStatistics(subStats);
		statRepo.save(stats);
		
		newUser.setSurname(user.getSurname());
		newUser.setName(user.getName());
		newUser.setEmail(user.getEmail());
		newUser.setNick(user.getNick());
		newUser.setBot(user.getBot());
		if(!cardId.isPresent()) {
			newUser.setCardId(user.getCardId());
		}else {
			return new ResponseEntity<>("CardID already exists!",HttpStatus.NOT_ACCEPTABLE);
		}

		newUser.setStatistics(stats);
		
		for (Iterator<User> i = userRepo.findAll().iterator(); i.hasNext();)
			if (user.getNick().equals(i.next().getNick()))
				return new ResponseEntity<>("User with nickname: " + user.getNick() + " already exists",
						HttpStatus.NOT_FOUND);

		userRepo.save(newUser);
		return new ResponseEntity<>("Created user: " + user.getNick(), HttpStatus.OK);
	}

	// adds list of users to specific team
	public ResponseEntity<String> addUsersToTeam(Integer id, String users) {
		Optional<Team> team = teamRepo.findById(id);
		team.get().setUsers(new ArrayList<User>());

		try {
			JSONArray listOfIncomingUsers = new JSONArray(users);
			for (int i = 0; i < listOfIncomingUsers.length(); i++) {
				JSONObject incomingUser = listOfIncomingUsers.getJSONObject(i);
				Optional<User> user = userRepo.findById(incomingUser.getInt("id"));
				team.get().getUsers().add(user.get());
			}
		} catch (JSONException e) {
			return new ResponseEntity<>("Users not found", HttpStatus.NOT_FOUND);
		}
		teamRepo.save(team.get());
		return new ResponseEntity<>("Users added to team: " + id, HttpStatus.OK);
	}

	// returns users that arent in specific team
	public ResponseEntity<List<User>> getUsersOutsideTeam(Integer id) {
		List<User> usersOustideTeam = new ArrayList<User>();
		for (Iterator<User> i = userRepo.findAll().iterator(); i.hasNext();)
			usersOustideTeam.add(i.next());

		Optional<Team> optionalTeam = teamRepo.findById(id);
		List<User> usersInTeam = optionalTeam.get().getUsers();

		for (int i = 0; i < usersInTeam.size(); i++) {
			for (int j = 0; j < usersOustideTeam.size(); j++) {
				if (usersInTeam.get(i).getID() == usersOustideTeam.get(j).getID()) {
					usersOustideTeam.remove(j);
				}
			}
		}

		if (usersOustideTeam.size() < 0) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<>(usersOustideTeam, HttpStatus.OK);
		}
	}

	// deletes specific user from specific team
	public ResponseEntity<String> deleteUserFromTeam(Integer userID, Integer teamID) {
		Optional<User> optionalUser = userRepo.findById(userID);
		Optional<Team> optionalTeam = teamRepo.findById(teamID);
		if (!optionalUser.isPresent() || !optionalTeam.isPresent()) {
			return new ResponseEntity<>("User: " + userID + " or Team: " + teamID + " Not found", HttpStatus.NOT_FOUND);
		} else {
			optionalTeam.get().getUsers().remove(optionalUser.get());
			userRepo.save(optionalUser.get());
			teamRepo.save(optionalTeam.get());
			return new ResponseEntity<>("Deleted user: " + userID + " from team: " + teamID, HttpStatus.OK);
		}
	}

	// adds specific user to specific team
	public ResponseEntity<String> addUserToTeam(Integer userID, Integer teamID) {
		Optional<User> optionalUser = userRepo.findById(userID);
		Optional<Team> optionalTeam = teamRepo.findById(teamID);
		if (!optionalUser.isPresent() || !optionalTeam.isPresent()) {
			return new ResponseEntity<>("User: " + userID + " or Team: " + teamID + " Not found", HttpStatus.NOT_FOUND);
		} else {
			optionalTeam.get().getUsers().add(optionalUser.get());
			userRepo.save(optionalUser.get());
			teamRepo.save(optionalTeam.get());
			return new ResponseEntity<>("Added user: " + userID + " to team: " + teamID, HttpStatus.OK);
		}
	}

	// returns all teams that contains specific user
	public ResponseEntity<List<Team>> getUserTeams(Integer id) {
		Optional<User> optionalUser = userRepo.findById(id);
		if (!optionalUser.isPresent()) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<>(optionalUser.get().getTeams(), HttpStatus.OK);
		}
	}

	// updates specific user
	public ResponseEntity<String> updateUser(Integer id, UserDTO user) {
		Optional<User> optionalUser = userRepo.findById(id);
		if(optionalUser.isPresent()) {
			User currentUser = optionalUser.get();
			currentUser.setSurname(user.getSurname());
			currentUser.setName(user.getName());
			currentUser.setEmail(user.getEmail());
			currentUser.setNick(user.getNick());
			currentUser.setCardId(user.getCardId());
			currentUser.setBot(user.getBot());
			userRepo.save(currentUser);
			return new ResponseEntity<>("User: " + id + " is correctly Updated", HttpStatus.OK);
		} else {
			return new ResponseEntity<>("Couldnt find user: " + id, HttpStatus.NOT_FOUND);
		}
	}

	// deletes specific user
	public ResponseEntity<String> deleteUser(Integer id) {
		Optional<User> optionalUser = userRepo.findById(id);
		if (!optionalUser.isPresent()) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} else {
			userRepo.deleteById(id);
			return new ResponseEntity<>("User: " + id + " is correctly deleted", HttpStatus.OK);
		}
	}
	
	public ResponseEntity<String> addTournamentWon(Integer userId, Integer tournamentType, Integer gameType){
		Optional<User> optionalUser= userRepo.findById(userId);
		if(!optionalUser.isPresent()) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} else {
			Statistics stat = optionalUser.get().getStatistics();
			SubStatistics subStat = null;
			for(SubStatistics subStats : stat.getSubStatistics()) {
				if(subStats.getTournamentType().equals(tournamentType))
					if(subStats.getGameType().equals(gameType))
						subStat = subStats;
			}
			stat.setTournamentWon(stat.getTournamentWon() + 1);
			subStat.setTournamentWon(subStat.getTournamentWon() + 1);
			
			userRepo.save(optionalUser.get());
			return new ResponseEntity<>("Statistics updated", HttpStatus.OK);
			
		}
	}
	
	public ResponseEntity<String> addTournamentPlayed(Integer userId, Integer tournamentType, Integer gameType){
		Optional<User> optionalUser= userRepo.findById(userId);
		if(!optionalUser.isPresent()) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);			
		} else {
			Statistics stat = optionalUser.get().getStatistics();
			SubStatistics subStat = null;
			for(SubStatistics subStats : stat.getSubStatistics()) {
				if(subStats.getTournamentType().equals(tournamentType))
					if(subStats.getGameType().equals(gameType))
						subStat = subStats;
			}
			stat.setTournamentPlayed(stat.getTournamentPlayed() + 1);
			subStat.setTournamentPlayed(subStat.getTournamentPlayed() + 1);
			
			userRepo.save(optionalUser.get());
			return new ResponseEntity<>("Statistics updated", HttpStatus.OK);
			
		}
		
	}
	
	public ResponseEntity<SubStatistics> findSubStatsByIds(Integer userId, Integer tournamentType, Integer gameType){
		Optional<User> optionalUser = userRepo.findById(userId);
		if(!optionalUser.isPresent()) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} else {
			SubStatistics subStat = null;
			List<SubStatistics> userSubStats = optionalUser.get().getStatistics().getSubStatistics();
			for(SubStatistics subStats : userSubStats) {
				if(subStats.getTournamentType().equals(tournamentType))
					if(subStats.getGameType().equals(gameType))
						subStat = subStats;
			}
			return new ResponseEntity<>(subStat, HttpStatus.OK);
		}
	}
	
	
	public ResponseEntity<Statistics> findStatsByUserId(Integer id){
		Optional<User> optionalUser = userRepo.findById(id);
		if(!optionalUser.isPresent()) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} else {
			Statistics userStats = optionalUser.get().getStatistics();
			return new ResponseEntity<>(userStats, HttpStatus.OK);
		}
	}

	// returns specific user
	public ResponseEntity<User> findById(Integer id) {
		Optional<User> optionalUser = userRepo.findById(id);
		if (!optionalUser.isPresent()) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<>(optionalUser.get(), HttpStatus.OK);
		}
	}

	// returns all users
	public ResponseEntity<Iterable<User>> getAllUsers() {
		Iterable<User> allUsers = userRepo.findAll();
		if (allUsers == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<>(allUsers, HttpStatus.OK);
		}
	}

	public ResponseEntity<User> getUserByCardId(String cardId){
		Optional<User> user= userRepo.findUserByCardId(cardId);
		if(user.isPresent()) {
			LOG.info(String.format("Get user by cardID: %s", user));
			return new ResponseEntity<>(user.get(),HttpStatus.OK);
		}else {
			LOG.info(String.format("Tried to get user by cardID: %s", user));
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}	
	}
	
	public ResponseEntity<User> getUserByNick(String nick){
		Optional<User> user= userRepo.findUserByNick(nick);
		if(user.isPresent()) {
			LOG.info(String.format("Get user by nick: %s", user));
			return new ResponseEntity<>(user.get(),HttpStatus.OK);
		}else {
			LOG.info(String.format("Tried to get user by nick: %s", user));
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}	
	}
	
	public ResponseEntity<List<User>> getBots(){
		Iterable<User> users = userRepo.findAll();
		List<User> bots = new ArrayList<>();
		for(User i : users) {
			if(i.getBot()==null) {
				
			}else if(!i.getBot().isEmpty()){
				bots.add(i);
			}
				
		}
		if(bots == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<>(bots, HttpStatus.OK);
		}
	}
	
	public ResponseEntity<String> getBotUrl(Integer id){
		Optional<User> optionalUser = userRepo.findById(id);
		if(optionalUser.isPresent()) {
			return new ResponseEntity<>(optionalUser.get().getBot(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);			
		}
		
	}
	
	public ResponseEntity<List<User>> getUserByContainNick(String nick){
		
		Iterable<User> user= userRepo.findAll();
		List<User> userByContainNick= new ArrayList<>();
		for(User i:user) {
			if(i.getNick().toLowerCase().contains(nick.toLowerCase())) {
				userByContainNick.add(i);
			}
		}
		if(!userByContainNick.isEmpty()) {
			LOG.info(String.format("Get user by contain nick: %s", userByContainNick));
			return new ResponseEntity<>(userByContainNick,HttpStatus.OK);
		}else {
			LOG.info(String.format("Tried to get user by contain nick: %s", userByContainNick));
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}	
	}
}
