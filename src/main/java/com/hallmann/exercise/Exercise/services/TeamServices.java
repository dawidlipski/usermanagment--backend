package com.hallmann.exercise.Exercise.services;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.hallmann.exercise.Exercise.DTO.TeamDTO;
import com.hallmann.exercise.Exercise.entities.Statistics;
import com.hallmann.exercise.Exercise.entities.SubStatistics;
import com.hallmann.exercise.Exercise.entities.Team;
import com.hallmann.exercise.Exercise.entities.User;
import com.hallmann.exercise.Exercise.repositories.TeamRepository;
import com.hallmann.exercise.Exercise.repositories.UserRepository;


@Service
public class TeamServices {

	@Autowired
	private TeamRepository teamRepo;

	@Autowired
	private UserRepository userRepo;

	private UserServices userService;

	// adds new team
	public ResponseEntity<String> addNewTeam(Team team) {
		Team newTeam = new Team();
		newTeam.setGroup_name(team.getGroup_name());
		for (Iterator<Team> i = teamRepo.findAll().iterator(); i.hasNext();)
			if (team.getGroup_name().equals(i.next().getGroup_name()))
				return new ResponseEntity<>("Team: " + team.getGroup_name() + " already exists", HttpStatus.NOT_FOUND);

		teamRepo.save(newTeam);
		return new ResponseEntity<>("Created team: " + team.getGroup_name(), HttpStatus.OK);
	}

	// updates specific team
	public ResponseEntity<String> updateTeam(Integer id, TeamDTO team) {
		Optional<Team> myTeam = teamRepo.findById(id);
			if (myTeam.isPresent()) {
				Team newTeam = new Team();
				newTeam.setGroup_name(team.getGroup_name());
				newTeam.setID(id);
				teamRepo.save(newTeam);
				return new ResponseEntity<>("Team: " + id + " is correctly Updated", HttpStatus.OK);
			}
		
		return new ResponseEntity<>("Couldnt find team: " + id, HttpStatus.NOT_FOUND);
	}

	public ResponseEntity<String> updateTeamStatsWon(Integer id, Integer tournamentType, Integer gameType) {
		Optional<Team> optionalTeam = teamRepo.findById(id);
		if (optionalTeam.isPresent()) {
			Team currentTeam = optionalTeam.get();
			List<User> users = currentTeam.getUsers();
			for (User u : users) {
				Statistics stat = u.getStatistics();
				SubStatistics subStat = null;
				for (SubStatistics subStats : stat.getSubStatistics()) {
					if (subStats.getTournamentType().equals(tournamentType))
						if (subStats.getGameType().equals(gameType))
							subStat = subStats;
				}
				stat.setTournamentWon(stat.getTournamentWon() + 1);
				subStat.setTournamentWon(subStat.getTournamentWon() + 1);
				userRepo.save(u);
			}
			return new ResponseEntity<>("Statistics updated", HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	public ResponseEntity<String> updateTeamStatsPlayed(Integer id, Integer tournamentType, Integer gameType) {
		Optional<Team> optionalTeam = teamRepo.findById(id);
		if (optionalTeam.isPresent()) {
			Team currentTeam = optionalTeam.get();
			List<User> users = currentTeam.getUsers();
			for (User u : users) {
				Statistics stat = u.getStatistics();
				SubStatistics subStat = null;
				for (SubStatistics subStats : stat.getSubStatistics()) {
					if (subStats.getTournamentType().equals(tournamentType))
						if (subStats.getGameType().equals(gameType))
							subStat = subStats;
				}
				stat.setTournamentPlayed(stat.getTournamentPlayed() + 1);
				subStat.setTournamentPlayed(subStat.getTournamentPlayed() + 1);
				userRepo.save(u);
			}
			return new ResponseEntity<>("Statistics updated", HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	// deletes specific team
	public ResponseEntity<String> deleteTeam(Integer id) {
		Optional<Team> optionalTeam = teamRepo.findById(id);
		if (!optionalTeam.isPresent()) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} else {
			teamRepo.deleteById(id);
			return new ResponseEntity<>("Team: " + id + " is correctly deleted", HttpStatus.OK);
		}
	}

	// returns list of users in specific team
	public ResponseEntity<List<User>> getTeamUsers(Integer id) {
		Optional<Team> optionalTeam = teamRepo.findById(id);
		if (!optionalTeam.isPresent()) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<>(optionalTeam.get().getUsers(), HttpStatus.OK);
		}
	}

	// returns specific team
	public ResponseEntity<Team> findById(Integer id) {
		Optional<Team> optionalTeam = teamRepo.findById(id);
		if (!optionalTeam.isPresent()) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<>(optionalTeam.get(), HttpStatus.OK);
		}
	}

	// returns all teams
	public ResponseEntity<Iterable<Team>> getAllUsers() {
		Iterable<Team> allTeams = teamRepo.findAll();
		if (allTeams == null) {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		} else {
			return new ResponseEntity<>(allTeams, HttpStatus.OK);
		}
	}

	public ResponseEntity<Integer> getTeamByUserList(List<Integer> userList) {

		Optional<User> user = userRepo.findById(userList.get(0));
		if (user.isPresent()) {
			if(userList.size()==1) {
				return new ResponseEntity<>(userList.get(0),HttpStatus.OK);
			}
			List<Team> firstUserTeams = user.get().getTeams();
			List<Integer> listForReturn= new ArrayList<>();
			List<Integer> users = new ArrayList<>();
			for (Team team : firstUserTeams) {
				for (User userInTeam : team.getUsers()) {
					users.add(userInTeam.getID());
				}
				if(userList.containsAll(users)) {
					listForReturn.add(team.getID());
				}
				users.removeAll(users);
			}
			return new ResponseEntity<>(listForReturn.get(0), HttpStatus.OK); 
		}else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND); 
		}
	}

	public ResponseEntity<String> addTeamWithUsers(Team team) {
		Team newTeam = new Team();
		newTeam.setGroup_name(team.getGroup_name());
		List<User> users= new ArrayList<>();
		
		Optional<Team> checkTeam = teamRepo.findTeamByGroupName(team.getGroup_name());
		if (checkTeam.isPresent())
			return new ResponseEntity<>("Team: " + team.getGroup_name() + " already exists", HttpStatus.NOT_FOUND);
		for(User user : team.getUsers()) {
			Optional<User> optionalUser = userRepo.findById(user.getID());
			if(!optionalUser.isPresent()) {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}else {
				users.add(new User(optionalUser.get()));
			}
				
		}
		newTeam.setUsers(users);
		teamRepo.save(newTeam);
		return new ResponseEntity<>("Created team: " + team.getGroup_name(), HttpStatus.OK);
	
	}






}
