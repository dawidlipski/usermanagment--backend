package com.hallmann.exercise.Exercise.entities;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

@Entity
public class SubStatistics {

	@Id
	@GeneratedValue
	private Integer ID;
	private Integer tournamentType;
	private Integer gameType;
	private Integer tournamentPlayed;
	private Integer tournamentWon;
	
	public SubStatistics() {
		tournamentPlayed = 0;
		tournamentWon = 0;
	}
	
	public SubStatistics(Integer tournamentType, Integer gameType) {
		this.tournamentType = tournamentType;
		this.gameType = gameType;
		tournamentPlayed = 0;
		tournamentWon = 0;
		
	}

	public Integer getID() {
		return ID;
	}

	public void setID(Integer iD) {
		ID = iD;
	}

	public Integer getTournamentType() {
		return tournamentType;
	}

	public void setTournamentType(Integer tournamentType) {
		this.tournamentType = tournamentType;
	}

	public Integer getGameType() {
		return gameType;
	}

	public void setGameType(Integer gameType) {
		this.gameType = gameType;
	}

	public Integer getTournamentPlayed() {
		return tournamentPlayed;
	}

	public void setTournamentPlayed(Integer tournamentPlayed) {
		this.tournamentPlayed = tournamentPlayed;
	}

	public Integer getTournamentWon() {
		return tournamentWon;
	}

	public void setTournamentWon(Integer tournamentWon) {
		this.tournamentWon = tournamentWon;
	}
	
	
}
