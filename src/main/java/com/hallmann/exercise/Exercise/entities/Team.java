package com.hallmann.exercise.Exercise.entities;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class Team {

	@Id
	@GeneratedValue
	private Integer ID;
	private String groupName;
	@ManyToMany
	private List<User> users;
	
	
	public Team(String name) {
		this.groupName = name;
	}

	public Team() {

	}

	public Integer getID() {
		return ID;
	}

	public void setID(Integer iD) {
		ID = iD;
	}

	public String getGroup_name() {
		return groupName;
	}

	public void setGroup_name(String groupName) {
		this.groupName = groupName;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

//	public Statistics getStatistics() {
//		return statistics;
//	}
//
//	public void setStatistics(Statistics statistics) {
//		this.statistics = statistics;
//	}

}
