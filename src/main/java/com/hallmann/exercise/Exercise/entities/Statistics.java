package com.hallmann.exercise.Exercise.entities;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Statistics {
	@Id
	@GeneratedValue
	private Integer ID;
	private Integer tournamentPlayed;
	private Integer tournamentWon;
	@OneToMany
	private List<SubStatistics> subStatistics;

	public Statistics() {
		tournamentPlayed = 0;
		tournamentWon = 0;
	}

	public Statistics(Integer tournamentPlayed, Integer tournamentWon) {
		this.tournamentPlayed = tournamentPlayed;
		this.tournamentWon = tournamentWon;
	}

	public Integer getID() {
		return ID;
	}

	public void setID(Integer iD) {
		ID = iD;
	}

	public Integer getTournamentPlayed() {
		return tournamentPlayed;
	}

	public void setTournamentPlayed(Integer tournamentPlayed) {
		this.tournamentPlayed = tournamentPlayed;
	}

	public Integer getTournamentWon() {
		return tournamentWon;
	}

	public void setTournamentWon(Integer tournamentWon) {
		this.tournamentWon = tournamentWon;
	}

	public List<SubStatistics> getSubStatistics() {
		return subStatistics;
	}

	public void setSubStatistics(List<SubStatistics> subStatistics) {
		this.subStatistics = subStatistics;
	}


}
