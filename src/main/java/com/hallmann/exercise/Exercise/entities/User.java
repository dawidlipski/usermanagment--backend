package com.hallmann.exercise.Exercise.entities;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;

import com.fasterxml.jackson.annotation.JsonIgnore;

@Entity
public class User {

	@Id
	@GeneratedValue
	private Integer ID;
	private String name;
	private String surname;
	private String email;
	private String nick;
	private String cardId;	
	private String bot;
	@ManyToMany(mappedBy = "users")
	@JsonIgnore
	private List<Team> teams;
	@OneToOne
	private Statistics statistics;

	public User(String name, String surname, String email, String nick, String cardId) {
		this.name = name;
		this.surname = surname;
		this.email = email;
		this.nick = nick;
		this.cardId= cardId;
	}

	public User() {

	}
	public User(User user) {
		this.ID= user.getID();
		this.name=user.getName();
		this.surname= user.getSurname();
		this.email=user.getEmail();
		this.nick= user.getNick();
		this.cardId=user.getCardId();
		this.bot=user.getBot();
		this.teams=user.getTeams();
	}
	

	public Integer getID() {
		return ID;
	}

	public void setID(Integer iD) {
		ID = iD;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNick() {
		return nick;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}

	public List<Team> getTeams() {
		return teams;
	}

	public void setTeams(List<Team> teams) {
		this.teams = teams;
	}

	public String getBot() {
		return bot;
	}

	public void setBot(String bot) {
		this.bot = bot;
	}
	
	public String getCardId() {
		return cardId;
	}

	public void setCardId(String cardId) {
		this.cardId = cardId;
	}

	public Statistics getStatistics() {
		return statistics;
	}

	public void setStatistics(Statistics statistics) {
		this.statistics = statistics;
	}
}
