package com.hallmann.exercise.Exercise.DTO;

import java.util.List;

import com.hallmann.exercise.Exercise.entities.SubStatistics;

public class StatisticsDTO {

	private Integer tournamentPlayed;
	private Integer tournamentWon;
	private List<SubStatistics> subStatistics;
	
	public StatisticsDTO() {
		
	}

	public StatisticsDTO(Integer tournamentPlayed, Integer tournamentWon) {
		this.tournamentPlayed = tournamentPlayed;
		this.tournamentWon = tournamentWon;
	}

	public Integer getTournamentPlayed() {
		return tournamentPlayed;
	}

	public void setTournamentPlayed(Integer tournamentPlayed) {
		this.tournamentPlayed = tournamentPlayed;
	}

	public Integer getTournamentWon() {
		return tournamentWon;
	}

	public void setTournamentWon(Integer tournamentWon) {
		this.tournamentWon = tournamentWon;
	}
	
	
	
	
	
}
