package com.hallmann.exercise.Exercise.DTO;

import java.util.List;

import com.hallmann.exercise.Exercise.entities.Statistics;
import com.hallmann.exercise.Exercise.entities.Team;

public class UserDTO {

	private String name;
	private String surname;
	private String nick;
	private String email;
	private String bot;
	private String cardId;	
	private List<Team> teams;
	private Statistics statistics;

	public UserDTO(String name, String surname, String nick, String email, String cardId) {
		this.name = name;
		this.surname = surname;
		this.nick = nick;
		this.email = email;
		this.cardId = cardId;
	}

	public List<Team> getTeams() {
		return teams;
	}

	public void setTeams(List<Team> teams) {
		this.teams = teams;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getNick() {
		return nick;
	}

	public void setNick(String nick) {
		this.nick = nick;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getBot() {
		return bot;
	}

	public void setBot(String bot) {
		this.bot = bot;
	}

	public Statistics getStatistics() {
		return statistics;
	}

	public void setStatistics(Statistics statistics) {
		this.statistics = statistics;
	}

	public String getCardId() {
		return cardId;
	}

	public void setCardId(String cardId) {
		this.cardId = cardId;
	}

}
