package com.hallmann.exercise.Exercise.DTO;

import java.util.List;

import com.hallmann.exercise.Exercise.entities.Statistics;
import com.hallmann.exercise.Exercise.entities.User;

public class TeamDTO {

	private String group_name;
	private List<User> users;
	private Statistics statistics;

	public TeamDTO() {

	}

	public TeamDTO(String groupName) {
		this.group_name = groupName;
	}

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public String getGroup_name() {
		return group_name;
	}

	public void setGroup_name(String groupName) {
		this.group_name = groupName;
	}

	public Statistics getStatistics() {
		return statistics;
	}

	public void setStatistics(Statistics statistics) {
		this.statistics = statistics;
	}

}
