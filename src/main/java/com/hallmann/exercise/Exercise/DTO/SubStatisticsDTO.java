package com.hallmann.exercise.Exercise.DTO;

public class SubStatisticsDTO {
	
	private Integer tournamentType;
	private Integer	gameType;
	private Integer tournamentPlayed;
	private Integer tournamentWon;
	
	public SubStatisticsDTO(){
		
	}

	public Integer getTournamentType() {
		return tournamentType;
	}

	public void setTournamentType(Integer tournamentType) {
		this.tournamentType = tournamentType;
	}

	public Integer getGameType() {
		return gameType;
	}

	public void setGameType(Integer gameType) {
		this.gameType = gameType;
	}

	public Integer getTournamentPlayed() {
		return tournamentPlayed;
	}

	public void setTournamentPlayed(Integer tournamentPlayed) {
		this.tournamentPlayed = tournamentPlayed;
	}

	public Integer getTournamentWon() {
		return tournamentWon;
	}

	public void setTournamentWon(Integer tournamentWon) {
		this.tournamentWon = tournamentWon;
	}
	
}
