package com.hallmann.exercise.Exercise.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.hallmann.exercise.Exercise.entities.User;

@Repository
public interface UserRepository extends CrudRepository<User, Integer> {
	Optional<User> findUserByCardId(String cardId);
	Optional<User> findUserByNick(String nick);
	
//	List<User> findById(Integer id);
}
