package com.hallmann.exercise.Exercise.repositories;

import java.util.List;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.hallmann.exercise.Exercise.entities.Team;

@Repository
public interface TeamRepository extends CrudRepository<Team, Integer> {
	Optional<Team> findTeamsByUsers(Integer userId);
	Optional<Team> findTeamByGroupName(String groupName);
}
