package com.hallmann.exercise.Exercise.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.hallmann.exercise.Exercise.entities.Statistics;

@Repository
public interface StatisticsRepository extends CrudRepository<Statistics, Integer>{

}
