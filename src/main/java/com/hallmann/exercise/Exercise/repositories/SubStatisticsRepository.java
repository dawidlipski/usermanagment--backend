package com.hallmann.exercise.Exercise.repositories;

import org.springframework.data.repository.CrudRepository;

import com.hallmann.exercise.Exercise.entities.SubStatistics;

public interface SubStatisticsRepository extends CrudRepository<SubStatistics, Integer>{

}
